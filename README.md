# Velkommen til kildekoden til norge.chat hjemmeside

* Siden er basert på det fantastiske prosjektet [Vitepress](https://vitepress.dev/).
* Vi blir veldig glade om du vil bidre til å gjøre denne nettsiden bedre.

## Slik kommer du igang med å utvikle

* Du må ha installert [Node.js](https://nodejs.org/) versjon 16 eller høyere, og git.
* Git clone dette repoet.
* Så kan du kjøre disse kommandoene:

```
cd norge.chat-webpage/
npm ci
```

For å kjøre nettsiden lokalt, kan du kjøre:

```
npm run docs:dev
```

Du kan da besøke nettsiden på [http://localhost:5173/](http://localhost:5173/)


## Litt om Vitepress
* Kildekoden til siden på nettsiden ligger i `/src` mappen.
* Du kan gjøre endringer her, samtidig som du kjører `npm run docs:dev`, du vil da se endringene på din lokale nettside.
* Config filen ligger i `/.vitepress`
* Om vi gjør endringer på dette repoet, blir det automatisk sendt til [norge.chat](https://norge.chat). Dette gjøres med Gitlab ci/cd.

# TODO (hjelp gjerne til):

* En blog inni denne siden med .rss, så brukere kan følge nettsiden. [Her](https://medium.com/@jeremy3/create-a-blog-with-vitepress-c4b1c203d688) er noen som har gjort lignende.
* En detaljert forklaring på hvordan man kan bruke noen av våre *ekstra* tjenester
    * Etherpad (sammarbeids tekst dokument)
    * Element (klient)
    * Hydrogen (klient)
    * Cinny (klient)
    * sliding sync
    * Push notification ntfy
    * hookshot (webhooks)
    