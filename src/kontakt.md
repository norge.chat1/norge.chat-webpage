---
layout: doc
sidebar: false
---

# Kontakt

* Mail: [kontakt@norge.chat](mailto:kontakt@norge.chat)
* Matrix: [#norge-chat:norge.chat](https://matrix.to/#/#norge-chat:norge.chat)