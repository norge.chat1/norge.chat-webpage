---
layout: doc
sidebar: false
---

# Endringslogg

## 07.08-24

* Oppdatert server til Debian 12
* Oppgradert Hetzner node til 8 core, 16 GB RAM.

## 06.12-23

* Slått av åpen registrering. Det trengs nå en token for å registrere seg. Den kan genereres av alle norge.chat brukere. Send melding `create` til brukeren `@registration-bot:norge.chat`

## 15.08-23

* Fjernet Dimension, da tjenesten [ikke blir oppdatert lengre](https://github.com/turt2live/matrix-dimension).
