---
layout: doc
sidebar: false
---

# Donasjoner

## Hvordan donere?

Send Vipps til Tlf nummer: 99156589 (Fredrik Fyksen)

# Liste over donasjoner:

* @ahaldorsen:norge.chat - 300 kr

## Kost pr måned
```
1 Server cx11 1 Server

1 € 3.2900 € 3.29001

Snapshot 2 Snapshots:
  Billed per GB per month
  Quantity type: GB/Months
  € 0.08051


Server cpx31 1 Server
  1 € 13.1000

2 IPs
  € 1.00001

Volume 1 Volume
  Billed per GB per month
  Quantity type: GB/Months
  (01/11/2023 - 30/11/2023)
  200 € 0.0440 € 8.80001
  Subtotal (excl. VAT) € 26.2705


Total including tax: € 32.84 pr month
```
