---
layout: doc
sidebar: false
---

# Bli medlem

Så hyggelig at du vil slenge deg med!

Vi har dessverre ikke åpen registrering lengre, så for å melde deg inn har du to muligheter:

1. Be en allerede medlem invitere deg.
2. Send en epost til [kontakt@norge.chat](mailto:kontakt@norge.chat), med emnefeltet: "jeg vil bli medlem", så får du en token i retur.

## Jeg er allerede medlem og vil invitere andre

* Send meldingen `create` til brukeren [registration-bot:norge.chat](https://matrix.to/#/@registration-bot:norge.chat)
* Du vil motta en token i retur, som den du ønsker å invitere kan bruke på [element.norge.chat/#/register](https://element.norge.chat/#/register)

## Hvorfor har vi ikke lengre åpen registrering

Vi hadde store problemer med at personer registrerte brukere som de lastet opp media som ikke var i tråd med våre regler.
